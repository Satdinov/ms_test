from django.test import TestCase

from shop.models import City, Street, Shop
from shop.tests.factories import CityFactory, StreetFactory, ShopFactory


class CityQuerySetTestCase(TestCase):
    def test_create_city(self):
        city = CityFactory()

        qs = City.objects.all()
        qs = qs.values_list('name', flat=True).order_by('name')

        self.assertEqual(list(qs), ['Город 0'])


class StreetQuerySetTestCase(TestCase):
    def test_create_street(self):
        street = StreetFactory()

        qs = Street.objects.all()
        qs = qs.values_list('name', flat=True).order_by('name')

        self.assertEqual(list(qs), ['Улица 1'])


class ShopQuerySetTestCase(TestCase):
    def test_create_shop(self):
        shop = ShopFactory()

        qs = Shop.objects.all()
        qs = qs.values_list('name', flat=True).order_by('name')

        self.assertEqual(list(qs), ['Магазин 0'])