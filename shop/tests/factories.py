import factory
import datetime


class CityFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'Город %d' % n)

    class Meta:
        model = 'shop.City'


class StreetFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'Улица %d' % n)
    city = factory.SubFactory(CityFactory)

    class Meta:
        model = 'shop.Street'


class ShopFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'Магазин %d' % n)
    city = factory.SubFactory(CityFactory)
    street = factory.SubFactory(StreetFactory)
    house_number = factory.Sequence(lambda n: n)
    opening_time = datetime.time(9, 00)
    close_time = datetime.time(18, 00)

    class Meta:
        model = 'shop.Shop'
